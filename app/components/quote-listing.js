import Ember from 'ember';

export default Ember.Component.extend({
 service: Ember.inject.service('quote-service'),
 isImageShowing: false,
 actions: {
    showResult() {
      this.set('isImageShowing', true);
      console.log(this.$());
      // check if the selected radio button is equal to the owner of the current call (there's got to be a better way)
      if(this.attrs.quote.value._internalModel._data.owner == $('input[name=option]:checked', this.element.children[1]).val()){
      	    this.set('isSucsess', true);
      	  } else {
      		this.set('isSucsess', false);
      	  }
    },
    resetResult() {
      this.set('isImageShowing', false);
    }
  }
});
