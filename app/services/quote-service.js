import Ember from 'ember';

export default Ember.Service.extend({
	quotes: null,
	init() {
    this._super(...arguments);
    this.set('quotes', []);
  },

  add(quote) {
    this.get('quotes').pushObject(quote);
  },

  remove(quote) {
    this.get('quotes').removeObject(quote);
  },

  empty() {
    this.get('quotes').setObjects([]);
  }

});
