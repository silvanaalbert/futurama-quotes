export default function() {  
  this.get('/quotes', function() {
    return {
      "quotes": [
          {
          id: 1,
          title: "Valentine’s Day’s coming? Aw crap! I forgot to get a girlfriend again!",
          owner: "Fry",
          image: "http://the-toast.net/wp-content/uploads/2015/07/Futurama_fry_looking_squint2.jpg"
          },
          {
          id: 2,
          title: "I was a hero to broken robots ‘cause I was one of them, but how can I sing about being damaged if I’m not? That’s like Christina Aguilera singing Spanish. Ooh, wait! That’s it! I’ll fake it!",
          owner: "Bender",
          image: "http://i1049.photobucket.com/albums/s395/joepoe1/Bender1ass.png"
          }, 
          {
          id: 3,
          title: "It’s like a party in my mouth and everybody’s throwing up!",
          owner: "Fry",
          image: "http://the-toast.net/wp-content/uploads/2015/07/Futurama_fry_looking_squint2.jpg"
          },
          {
          id: 4,
          title: "Good news, everyone! I’ve taught the toaster to feel love!",
          owner: "Farnsworth",
          image: "http://pinso.co.uk/wp-content/uploads/2013/05/futurama-professor-farnsworth-Pinso-1-e1369984431328.jpg"
          },
          {
          id: 5,
          title: "Game’s over, losers! I have all the money. Compare your lives to mine and then kill yourselves.",
          owner: "Bender",
          image: "http://i1049.photobucket.com/albums/s395/joepoe1/Bender1ass.png"
          }, 
          {
          id: 6,
          title: "This is the worst kind of discrimination there is: the kind against me!",
          owner: "Bender",
          image: "http://i1049.photobucket.com/albums/s395/joepoe1/Bender1ass.png"
          }, 
          {
          id: 7,
          title: "I am Lrrr! Ruler of the planet Omicron Persei 8! May I crash on your couch?",
          owner: "Lrr",
          image: "http://static.episode39.it/character/5065.jpg?t=1283602545"
          }, 
          {
          id: 8,
          title: "This is ancient Earth's most foolish program. Why does Ross, the largest friend, not simply eat the other five?",
          owner: "Lrr",
          image: "http://static.episode39.it/character/5065.jpg?t=1283602545"
          }, 
          {
          id: 9,
          title: "My folks were always on me to groom myself and wear underpants. What am I, the pope?",
          owner: "Fry",
          image: "http://the-toast.net/wp-content/uploads/2015/07/Futurama_fry_looking_squint2.jpg"
          },
          {
          id: 9,
          title: "Your music’s bad and you should feel bad!",
          owner: "Zoidberg",
          image: "http://cdn.instructables.com/FVZ/OO6C/H7ANSLLA/FVZOO6CH7ANSLLA.MEDIUM.jpg"
          },
          // {
          // id: 10,
          // title: "I guess if you want children beaten, you have to do it yourself!",
          // owner: "Bender",
          // image: "http://i1049.photobucket.com/albums/s395/joepoe1/Bender1ass.png"
          // },
          // {
          // id: 11,
          // title: "You know what cheers me up? Other people’s misfortune.",
          // owner: "Bender",
          // image: "http://i1049.photobucket.com/albums/s395/joepoe1/Bender1ass.png"
          // }
        ]
      }
  });
}

